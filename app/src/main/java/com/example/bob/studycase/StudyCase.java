package com.example.bob.studycase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class StudyCase extends AppCompatActivity {

    EditText panjang,lebar;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_case);
        panjang = findViewById(R.id.panjang);
        lebar = findViewById(R.id.lebar);
        hasil = findViewById(R.id.hasil);
        //test kedua

    }

    public void hitung(View view){
        if (panjang.getText().toString().equalsIgnoreCase("")){
            panjang.requestFocus();
        }else if (lebar.getText().toString().equalsIgnoreCase("")){
            lebar.requestFocus();
        }else {
            int result = Integer.parseInt(panjang.getText().toString()) * Integer.parseInt(lebar.getText().toString());
            hasil.setText("" + result);
        }
    }
}
